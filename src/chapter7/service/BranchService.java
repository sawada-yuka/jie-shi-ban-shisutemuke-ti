package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.Branch;
import chapter7.dao.BranchDao;

public class BranchService {

    public void register(Branch branch) {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao BranchDao = new BranchDao();
            BranchDao.insert(connection, branch);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Branch> getBranch() {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao BranchDao = new BranchDao();
            List<Branch> ret = BranchDao.getBranchAll(connection);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
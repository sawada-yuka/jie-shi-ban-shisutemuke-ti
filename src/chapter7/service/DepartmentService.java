package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.Department;
import chapter7.dao.DepartmentDao;

public class DepartmentService {

    public void register(Department department) {

        Connection connection = null;

        try {
            connection = getConnection();

            DepartmentDao DepartmentDao = new DepartmentDao();
            DepartmentDao.insert(connection, department);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Department> getDepartment() {

        Connection connection = null;

        try {
            connection = getConnection();

            DepartmentDao DepartmentDao = new DepartmentDao();
            List<Department> ret =DepartmentDao.getDepartmentAll(connection);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
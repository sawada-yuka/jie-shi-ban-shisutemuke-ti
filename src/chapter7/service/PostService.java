package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Post;
import chapter7.beans.UserPost;
import chapter7.dao.PostDao;
import chapter7.dao.UserPostDao;

public class PostService {

    public void register(Post post) {

        Connection connection = null;

        try {
            connection = getConnection();

            PostDao PostDao = new PostDao();
            PostDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserPost> getPost(String startDate,String endDate,String category) {

    	Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        String currentTimestampToString = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(currentTimestamp);

    	if(!StringUtils.isEmpty(startDate)) {
    		startDate = startDate + " 00:00:00";
    	} else {
    		startDate = "2019-01-01 00:00:00";
    	}
    	if(!StringUtils.isEmpty(endDate)) {
    		endDate = endDate + " 23:59:59";
    	} else {
    		endDate = currentTimestampToString;
    	}

        Connection connection = null;

        try {
            connection = getConnection();

            UserPostDao userPostDao = new UserPostDao();
			List<UserPost> ret = userPostDao.getUserPosts(connection, startDate, endDate, category);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(Post post) {

        Connection connection = null;

        try {
            connection = getConnection();

            PostDao PostDao = new PostDao();
            PostDao.delete(connection , post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}

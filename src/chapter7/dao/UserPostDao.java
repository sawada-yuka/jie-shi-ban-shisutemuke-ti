package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserPost;
import chapter7.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserPosts(Connection connection, String start, String end, String category) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.title as title, ");
            sql.append("posts.text as text, ");
            sql.append("posts.category as category, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE posts.created_date >= ? ");
            sql.append("AND posts.created_date <= ? ");
            if(category != null) {
            	if(!category.equals("")) {
            		 sql.append("AND category LIKE ? ");
            	}
            }
            sql.append("ORDER BY created_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1,start);
            ps.setString(2,end);
            if(category != null) {
            	if(!category.equals("")) {
            		ps.setString(3,"%"+ category + "%");
            	}
            }

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();

        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost post = new UserPost();
                post.setId(id);
                post.setUser_id(user_id);
                post.setTitle(title);
                post.setText(text);
                post.setCategory(category);
                post.setName(name);
                post.setCreated_date(createdDate);

                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
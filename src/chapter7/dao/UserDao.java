package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("id");
            sql.append(", login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_working");
            sql.append(") VALUES (");
            sql.append("?"); // id
            sql.append(", ?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", ?"); // is_working
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());//.toStringでString型に変えている

            ps.setInt(1, user.getId());
            ps.setString(2, user.getLogin_id());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getName());
            ps.setInt(5, user.getBranch_id());
            ps.setInt(6, user.getDepartment_id());
            ps.setInt(7, 1);
            ps.executeUpdate();//or ps.executeクエリ(Selectの時) 何個目の？にどのデータを入れるか
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public User getUser(Connection connection, String login_id,
	        String password) {

	    PreparedStatement ps = null;

	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, login_id);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public User getUserId(Connection connection, String login_id) {

	    PreparedStatement ps = null;

	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, login_id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public List<User> getUserAll(Connection connection) {

	    PreparedStatement ps = null;

	    try {
	        String sql = "SELECT * FROM users";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty() == true) {
	            return null;
	        } else {
	            return userList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();

	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String login_id = rs.getString("login_id");
	            String password = rs.getString("password");
	            String name = rs.getString("name");
	            int branch_id  = rs.getInt("branch_id");
	            int department_id = rs.getInt("department_id");
	            int is_working = rs.getInt("is_working");

	            User user = new User();
	            user.setId(id);
	            user.setLogin_id(login_id);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranch_id(branch_id);
	            user.setDepartment_id(department_id);
	            user.setIs_working(is_working);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public void userupdate(Connection connection, User user) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "UPDATE users SET is_working = ? WHERE id = ?";

    		ps = connection.prepareStatement(sql.toString());
    		System.out.println(sql);
    		ps.setInt(1, user.getIs_working());
    		ps.setInt(2, user.getId());
    		ps.executeUpdate();

    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    	    close(ps);
    	}
    }

	public User getUser(Connection connection, int id) {

	    PreparedStatement ps = null;

	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public void update(Connection connection, User user) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            if(user.getPassword().isEmpty() == false) {
            	sql.append(", password = ?");
            }
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", department_id = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            if(user.getPassword().isEmpty() == false) {
        	   ps.setString(2, user.getPassword());
        	   ps.setString(3, user.getName());
               ps.setInt(4, user.getBranch_id());
        	   ps.setInt(5, user.getDepartment_id());
        	   ps.setInt(6, user.getId());
            } else {
        	   ps.setString(2, user.getName());
               ps.setInt(3, user.getBranch_id());
        	   ps.setInt(4, user.getDepartment_id());
        	   ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
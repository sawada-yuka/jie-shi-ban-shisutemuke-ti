package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.Department;
import chapter7.exception.SQLRuntimeException;

public class DepartmentDao {

    public void insert(Connection connection, Department department) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO departments ( ");
            sql.append("name");
            sql.append(") VALUES (");
            sql.append(" ?"); // name
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, department.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Department> getDepartmentAll(Connection connection) {

	    PreparedStatement ps = null;

	    try {
	        String sql = "SELECT * FROM departments";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<Department> departmentList = toDepartmentList(rs);

	        if (departmentList.isEmpty() == true) {
	            return null;
	        } else {
	            return departmentList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException {

	    List<Department> ret = new ArrayList<Department>();

	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String name = rs.getString("name");

	            Department department = new Department();
	            department.setId(id);
	            department.setName(name);
	            ret.add(department);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}
}

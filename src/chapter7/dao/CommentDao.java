package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter7.beans.Comment;
import chapter7.exception.SQLRuntimeException;

public class CommentDao {

	 public void insert(Connection connection, Comment comment) {

	        PreparedStatement ps = null;

	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO comments ( ");
	            sql.append("text");
	            sql.append(", post_id");
	            sql.append(", user_id");
	            sql.append(") VALUES (");
	            sql.append(" ?"); // text
	            sql.append(", ?"); // post_id
	            sql.append(", ?"); // user_id
	            sql.append(")");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, comment.getText());
	            ps.setInt(2, comment.getPost_id());
	            ps.setInt(3, comment.getUser_id());
	            ps.executeUpdate();

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	 public void commentdelete(Connection connection, Comment comment) {

	    	PreparedStatement ps = null;

	    	try {
	    		String sql = "DELETE FROM comments WHERE id = ?";
	    		ps = connection.prepareStatement(sql.toString());
	    		ps.setInt(1, comment.getId());
	    		ps.executeUpdate();
	    	} catch (SQLException e) {
	    		throw new SQLRuntimeException(e);
	    	} finally {
	    	    close(ps);
	    	}
	 }
}

package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<String>();

    	User users = new User();

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");

        users.setLogin_id(login_id);

        LoginService loginService = new LoginService();
        User user = loginService.login(login_id, password);

        HttpSession session = request.getSession();

        if (user != null && user.getIs_working() == 1) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");

        } else {

            if (StringUtils.isEmpty(login_id) == true) {
                messages.add("ログインIDを入力してください");
            } else if (StringUtils.isBlank(login_id) == true) {
                messages.add("ログインIDを入力してください");
            }
            if (StringUtils.isEmpty(password) == true) {
                messages.add("パスワードを入力してください");
            } else if (StringUtils.isBlank(password) == true) {
                messages.add("パスワードを入力してください");
            } else {
            messages.add("ログインに失敗しました。");
            }
            session.setAttribute("errorMessages", messages);
            request.setAttribute("users", users);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

}
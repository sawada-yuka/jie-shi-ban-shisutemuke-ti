package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.User;
import chapter7.beans.UserComment;
import chapter7.beans.UserPost;
import chapter7.service.CommentService;
import chapter7.service.PostService;
import chapter7.service.UserService;



@WebServlet(urlPatterns = { "/index.jsp" })//index.jspはホーム画面のこと
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        UserPost serchPost = new UserPost();

        String startDate = request.getParameter("startdate");
        String endDate = request.getParameter("enddate");
        String category = request.getParameter("category");

        serchPost.setStartdate(startDate);
        serchPost.setEnddate(endDate);
        serchPost.setCategory(category);

    	request.setAttribute("serchPost", serchPost);

        List<User> users = new UserService().getUser();
        request.setAttribute("users",users);

        List<UserPost> posts = new PostService().getPost(startDate,endDate,category);
        request.setAttribute("posts", posts);

        List<UserComment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);
        request.setAttribute("isShowMessageForm", isShowMessageForm);

    	request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    }
}

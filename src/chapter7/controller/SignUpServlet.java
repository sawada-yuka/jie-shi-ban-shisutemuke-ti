package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Branch;
import chapter7.beans.Department;
import chapter7.beans.User;
import chapter7.service.BranchService;
import chapter7.service.DepartmentService;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/signup" })

public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branches = new BranchService().getBranch();
    	request.setAttribute("branches",branches);

    	List<Department> departments = new DepartmentService().getDepartment();
    	request.setAttribute("departments",departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

            new UserService().register(user);

            response.sendRedirect("usermanage");
        } else {

        	User user = new User();

        	String login_id = request.getParameter("login_id");
        	String name = request.getParameter("name");
        	int branch_id = Integer.parseInt(request.getParameter("branch_id"));
        	int department_id = Integer.parseInt(request.getParameter("department_id"));
        	String branch_name = request.getParameter("branch_name");
        	String department_name = request.getParameter("department_name");

        	user.setLogin_id(login_id);
        	user.setName(name);
        	user.setBranch_id(branch_id);
        	user.setDepartment_id(department_id);
        	user.setBranch_name(branch_name);
        	user.setDepartment_name(department_name);

        	request.setAttribute("user", user);
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String login_id = request.getParameter("login_id");
        int branch_id = Integer.parseInt(request.getParameter("branch_id"));
    	int department_id = Integer.parseInt(request.getParameter("department_id"));
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");

        List<Branch> branches = new BranchService().getBranch();
    	request.setAttribute("branches",branches);
    	List<Department> departments = new DepartmentService().getDepartment();
    	request.setAttribute("departments",departments);

        UserService userService = new UserService();
        User userId = userService.login(login_id);

        if(userId != null) {
        	messages.add("既に使用されているログインIDです");
        }
        if (StringUtils.isEmpty(login_id) == true) {
        	messages.add("ログインIDを入力してください");
        }else if(!login_id.matches("^[0-9a-zA-Z]{6,20}+$")) {
        	messages.add("ログインIDは6文字以上20文字以下の半角英数字で入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else if(StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        } else if(!password.matches("^[ -~｡-ﾟ]{6,20}+$")) {
        	messages.add("パスワードは6文字以上20文字以下の半角文字で入力してください");
        }
        if (StringUtils.isEmpty(password2) == true) {
            messages.add("パスワード(確認)を入力してください");
        } else if (StringUtils.isBlank(password2) == true) {
            messages.add("パスワード(確認)を入力してください");
        } else if(!password2.matches("^[ -~｡-ﾟ]{6,20}+$")) {
        	messages.add("パスワード(確認)は6文字以上20文字以下の半角文字で入力してください");
        }
        if(!password.equals(password2)) {
        	messages.add("パスワードが一致していません");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        } else if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if (10 < name.length()) {
        	messages.add("名前は10文字以下で入力してください");
        }
        if(branch_id == 1) {
         	if(!(department_id == 1 || department_id == 2)) {
         		messages.add("支店と部署・役職が無効な組み合わせです");
         	}
         }
         if(branch_id == 2) {
         	if(!(department_id == 3 || department_id == 4)) {
         		messages.add("支店と部署・役職が無効な組み合わせです");
         	}
         }
         if(branch_id == 3) {
         	if(!(department_id == 3 || department_id == 4)) {
         		messages.add("支店と部署・役職が無効な組み合わせです");
         	}
         }
         if(branch_id == 4) {
         	if(!(department_id == 3 || department_id == 4)) {
         		messages.add("支店と部署・役職が無効な組み合わせです");
         	}
         }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
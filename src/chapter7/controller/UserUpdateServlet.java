package chapter7.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.User;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/userupdate" })
public class UserUpdateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = new User();
        user.setIs_working(Integer.parseInt(request.getParameter("is_working")));
        user.setId(Integer.parseInt(request.getParameter("id")));

        new UserService().userupdate(user);

        response.sendRedirect("usermanage");
    }
}

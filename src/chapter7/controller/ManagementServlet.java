package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.Branch;
import chapter7.beans.Department;
import chapter7.beans.User;
import chapter7.service.BranchService;
import chapter7.service.DepartmentService;
import chapter7.service.UserService;

@WebServlet(urlPatterns = { "/usermanage" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

            User user = new User();
            user.setId(Integer.parseInt(request.getParameter("id")));
            user.setName(request.getParameter("name"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

            new UserService().register(user);

            Department department = new Department();
            department.setId(Integer.parseInt(request.getParameter("id")));
            department.setName(request.getParameter("name"));

            new DepartmentService().register(department);

            Branch branch = new Branch();
            branch.setId(Integer.parseInt(request.getParameter("id")));
            branch.setName(request.getParameter("name"));

            new BranchService().register(branch);

            response.sendRedirect("./");


    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<User> users = new UserService().getUser();
        List<Department> departments = new DepartmentService().getDepartment();
        List<Branch> branches = new BranchService().getBranch();

        request.setAttribute("users",users);
        request.setAttribute("departments",departments);
        request.setAttribute("branches",branches);
    	request.getRequestDispatcher("/usermanage.jsp").forward(request, response);
    }
}

package chapter7.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.User;

@WebFilter(urlPatterns= {"/usermanage","/index.jsp","/post","/signup","/edituser"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest)request;

		User user = (User) req.getSession().getAttribute("loginUser");

		if (user == null) {
			List<String> messages = new ArrayList<String>();
            messages.add("ログインしてください。");
            req.getSession().setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("login");
			return;
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}

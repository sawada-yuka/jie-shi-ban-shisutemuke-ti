package chapter7.beans;

import java.io.Serializable;
import java.util.Date;

public class UserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private String title;
    private String text;
    private String category;
    private String name;
    private Date created_date;
    private String startdate;
    private String enddate;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getStartDate() {
		return startdate;
	}
	public void setStartdate(String startDate) {
		this.startdate = startDate;
	}
	public String getEndDate() {
		return enddate;
	}
	public void setEnddate(String endDate) {
		this.enddate = endDate;
	}


}
package chapter7.beans;

import java.io.Serializable;

public class UserBranch implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private String name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
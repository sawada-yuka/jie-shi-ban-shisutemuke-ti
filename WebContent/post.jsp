<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
<title>新規投稿</title>
<link rel="stylesheet" href="post.css">
</head>
<body>

<header><h1>新規投稿</h1></header>
    	<a href="./" class="btn" >ホーム</a><br /><br />

<div class="form-area">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<font color="#cd5c5c">※<c:out value="${message}" /></font><br>
                </c:forEach>
        </div>
        <c:remove var="errorMessages" scope="session"/>
    </c:if><br>

    	<form action="post" method="post">

    		<label for="title">件名</label><br />
                <input type="text" name="title" value="${post.title}" size="39" placeholder="30文字以内"> <br /><br />
            <label for="text">本文</label><br />
               <textarea name="text" cols="40" rows="5" placeholder="1000文字以内">${post.text}</textarea> <br /><br />
            <label for="category">カテゴリー</label><br />
               <input type="text" name="category" value="${post.category}" size="39" placeholder="10文字以内"> <br /><br />
            <button class="button" type="submit">post</button><br /><br />


        </form>
</div>
</body>
</html>
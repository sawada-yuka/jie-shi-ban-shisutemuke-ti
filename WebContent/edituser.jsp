<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
<title>ユーザーの編集</title>
<link rel="stylesheet" href="edituser.css">
</head>
<body>

<header><h1>ユーザーの編集</h1></header>
   	<a href="usermanage" class="btn">ユーザー管理</a>
    <a href="./" class="btn">ホーム</a><br /><br /><br>

<div class="main-contents">
    <c:if test="${ not empty errorMessages }">
    	<div class="errorMessages">
            	<c:forEach items="${errorMessages}" var="message">
            		<font color="#cd5c5c">※<c:out value="${message}" /></font><br>
                </c:forEach>
        </div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

   	<form action="edituser" method="post">

   		<input name="id" value="${editUser.id}" id="id" type="hidden"/>
   		<label for="login_id">ログインID</label><br />
               <input type="text" placeholder="6字以上20字以内" name="login_id" value="${editUser.login_id}" size="15" > <br /><br />
           <div class="form-group">
           <label for="password">パスワード</label><br />
               <input type="password" name="password" size="15" placeholder="6字以上20字以内"><br /><br />
           </div>
           <div class="form-group">
           <label for="password2">パスワード(確認)</label><br />
               <input type="password" name="password2" size="15" placeholder="6字以上20字以内"><br /><br />
           </div>
           <label for="name">名前</label><br />
              <input type="text" name="name" value="${editUser.name}" size="15" placeholder="10字以内"><br /><br />

           <c:if test="${ loginUser.id != editUser.id }">
           <label for="branch_id">支店</label><br />
              <select name="branch_id">
				<c:forEach items="${branches}" var="branch">
            		<c:choose>
						<c:when test="${ editUser.branch_id == branch.id }">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${branch.id}">${branch.name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br /><br />
            <label for="department_id">部署・役職</label><br />
			<select name="department_id">
				<c:forEach items="${departments}" var="department">
            		<c:choose>
						<c:when test="${ editUser.department_id == department.id }">
							<option value="${department.id}" selected>${department.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${department.id}">${department.name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br /><br />
			</c:if >
			<c:if test="${ loginUser.id == editUser.id }">
				<input name="department_id" value="${editUser.department_id}" id="id" type="hidden"/>
				<input name="branch_id" value="${editUser.branch_id}" id="id" type="hidden"/>
			</c:if>
            <button class="button" type="submit">edit</button><br /><br />

       </form>
</div>
</body>
</html>
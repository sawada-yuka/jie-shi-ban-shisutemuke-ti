<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet" href="signup.css">
</head>


<body>

<header><h1>ユーザー新規登録</h1></header>
    	 <a href="usermanage" class="btn">ユーザー管理</a>
    	 <a href="./" class="btn">ホーム</a><br /><br /><br>

<div class="form-area">
    <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                        <c:forEach items="${errorMessages}" var="message">
                            <font color="#cd5c5c">※<c:out value="${message}" /></font><br>
                        </c:forEach><br>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

    	<form action="signup" method="post">
    		<label for="login_id">ログインID</label><br />
                <input type="text" name="login_id" value="${user.login_id}" placeholder="6字以上20字以内"　size="15" > <br /><br />
            <label for="password">パスワード</label><br />
                <input type="password" name="password" size="15" placeholder="6字以上20字以内"><br /><br />

            <label for="password2">パスワード(確認)</label><br />
                <input type="password" name="password2" size="15" placeholder="6字以上20字以内"><br /><br />
            <label for="name">名前</label><br />
               <input type="text" name="name" value="${user.name}" size="15" placeholder="10字以内"> <br /><br />
            <label for="branch_id">支店</label><br />
            	<select name="branch_id">
            	<c:forEach items="${branches}" var="branch">
				<option value="${branch.id}">${branch.name}</option>
				</c:forEach></select><br /><br />
            <label for="department_id">部署・役職</label><br />
            <select name="department_id">
            	<c:forEach items="${departments}" var="department">
				<option value="${department.id}">${department.name}</option>
				</c:forEach></select><br /><br />
            <button class="button" type="submit">register</button><br /><br />

        </form>
</div>
</body>
</html>

<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
<link rel="stylesheet" href="login.css">
</head>
<body>

<div class="main-contents">

            <form action="login" method="post">
            <div class="header" style="text-align: center;">
            <c:if test="${ not empty errorMessages }">
 		<div class="errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<font color="#cd5c5c">※<c:out value="${message}" /></font><br/>
               </c:forEach>
        </div>
		<c:remove var="errorMessages" scope="session"/>
    </c:if>
	<h1><big>掲示板</big></h1>
</div>

                <label for="login_id">ログインID</label><br/>
                <input name="login_id" value="${users.login_id}" id="login_id"/> <br />
                <label for="password">パスワード</label><br/>
                <input name="password" type="password" id="password"/> <br /><br />
                <input type="submit" class="login" value="Login" /> <br />
            </form>
</div>
</body>
</html>
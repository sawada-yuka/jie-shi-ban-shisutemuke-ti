<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet" href="usermanage.css">
<head>
<meta http-equiv="Content-Type" content="text/html;charset="UTF-8">
<title>ユーザー管理画面</title>
</head>

<script>
function revival(){
	var check = confirm("アカウントを復活してよろしいですか？");
	return check;
}
function stop(){
	var check = confirm("アカウントを停止してよろしいですか？");
	return check;
}
</script>

<body>



<header><h1>ユーザー管理画面</h1></header>
 <div class="main-contents">

 	<c:if test="${ not empty errorMessages }">
    	<div class="errorMessages">
            	<c:forEach items="${errorMessages}" var="message">
                	<font color="#cd5c5c">※<c:out value="${message}" /></font><br>
                </c:forEach><br>

        </div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

        <div class="header">
        	<a href="signup"  class="btn">新規ユーザ登録</a>
            <a href="./" class="btn">ホーム</a>
        </div><br />



        <div class="usermanage">
			<c:forEach items="${users}" var="user">
	            <div class="user">
	            	<label>ログインID</label>
	            		<div class="login_id"><c:out value="${user.login_id}" /></div>
	            	<label>名前</label>
	                	<div class="name"><c:out value="${user.name}" /></div>
	                <c:forEach items="${branches}" var="branch">
		                <c:if test="${ user.branch_id == branch.id }">
		                <label>支店</label>
		                	<div class="branch_name"><c:out value="${branch.name}" /></div>
		                </c:if>
	                </c:forEach>
	                <c:forEach items="${departments}" var="department">
		                <c:if test="${ user.department_id == department.id }">
		                <label>部署・役職</label>
		                	<div class="department_name"><c:out value="${department.name}" /></div>
		                </c:if>
	                </c:forEach><br />




		           	<c:choose>
			            <c:when test="${ user.is_working == 0 }">
			            <c:if test="${ loginUser.id != user.id }">

			            	<form action="userupdate" method="post">
			            	<input name="is_working" value="1" id="is_working" type="hidden"/>
			            	<input name="id" value="${user.id}" id="id" type="hidden"/>
			            		<input id="btn" name="btn" type="submit" value="復活" onClick= "return revival()" style="float: right;"></form>
			            </c:if>
			            </c:when>
			            <c:otherwise>
			            	<c:if test="${ loginUser.id != user.id }">
			            	<form action="userupdate" method="post">
			            	<input name="is_working" value="0" id="is_working" type="hidden"/>
			            	<input name="id" value="${user.id}" id="id" type="hidden"/>
			            		<input id="btn" name="btn" type="submit" value="停止" onClick= "return stop()" style="float: right;"></form>
			            	</c:if>
			            </c:otherwise>
		            </c:choose>
	            	<div class="edituser">
	            	<form action="edituser" method="get">
			            <input name="id" value="${user.id}" id="id" type="hidden"/>
			            <input type="submit" value="編集" style="float: right;">
			        </form>
	            	</div>
            	</div>
            	</c:forEach><br /><br />
	         <a href="./"  class="btn">ホーム画面へ</a><br />
		</div>
</div>
</body>
</html>
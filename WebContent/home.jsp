<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
<link rel="stylesheet" href="home

.css">
</head>

<script>
function deleteCheck(){
	var check = confirm("本当に削除してよろしいですか？");
	return check;
}
</script>

<body>
<br>

	<div class="header" style="text-align: center;">
	<h1><big><big>掲示板</big></big></h1>
	<a href="post" class="btn">新規投稿</a>
            <c:if test="${ (loginUser.branch_id)  == 1 && (loginUser.department_id == 1) }">
            	<a href="usermanage" class="btn">ユーザー管理</a>
            </c:if>
    <a href="login" class="btn" >ログアウト</a><br><br><br>

	<hr class="fade"><br><br><br><br><br>
	</div>





<div class="main-contents">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<font color="#cd5c5c">※<c:out value="${message}" /></font><br>
                </c:forEach>
        </div>
        <c:remove var="errorMessages" scope="session"/>
    </c:if>

        <div class="posts" >
        	<form action="./" method="get" style="text-align: right;">
        		<label for="start"></label>
					<input type="date" name="startdate"  value="${serchPost.startDate}">
				<label for="end">～</label>
					<input type="date" name="enddate"  value="${serchPost.endDate}"><br />
				<label for="category"></label>
					<input type="search" name="category" value="${serchPost.category}" placeholder="カテゴリー"><br>
					<input type="submit" value="検索"><br />
			</form><br>

			<c:forEach items="${posts}" var="post">
	            <div class="post" style="text-align: left;">
					<input name="user_id" value="${post.user_id}" id="user_id" type="hidden"/>

						<div class="name"><font size="5"><c:out value="${post.name}" /></font>&nbsp;さんの投稿</div><br>

	            	<label><big>件名</big></label>
	            		<div class="title"><c:out value="${post.title}" /></div><br>
	            	<label><big>本文</big></label>
	            	<c:forEach var="text" items="${fn:split(post.text, '
	            	')}">
	                	<div class="text" ><c:out value="${text}" /></div>
	                </c:forEach><br>
	                <label><big>カテゴリー</big></label>
	                <div class="category"><c:out value="${post.category}" /></div><br>
	                <div class="date" style="float: right;">登録日時：<fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br><br>
	                <c:if test="${ loginUser.id == post.user_id }">
							<div class="delete" style="float: right;">
								<form action="delete" method="post">
									<input name="id" value="${post.id}" id="id" type="hidden"/>
									<input type="submit" value="削除" onClick= "return deleteCheck()">
								</form>
							</div><br><br><br>
						</c:if>



					<div class="form-area" style="text-align: right;">
					<c:if test="${ isShowMessageForm }">
						<form action="comment" method="post">
		            		<input name="post_id" value="${post.id}" id="post_id" type="hidden"/>
		            		<textarea name="comment" cols="40" rows="4" class="comment-box" placeholder="コメントを入力してください(500文字以内)"></textarea><br>
		            		 <button class="button" type="submit" >send</button>
		        		</form>
		   			 </c:if>
					</div>

					<div class="comments">
					<c:forEach items="${comments}" var="comment">
					<c:if test="${ post.id == comment.post_id }">
			            <div class="comment" style="text-align: left;">
			            	<input name="user_id" value="${comment.user_id}" id="user_id" type="hidden"/>

								<div class="name"><font size="4"><c:out value="${comment.name}" /></font>&nbsp;さんのコメント</div><br>
			            	<label>本文</label>
			            		<c:forEach var="text" items="${fn:split(comment.text, '
	            				')}">
	                				<div class="text"><c:out value="${text}"/></div>
	                			</c:forEach>
							<div class="date" style="text-align: right;"><small>登録日時：<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></small></div>
							<c:if test="${ loginUser.id == comment.user_id }">
							<div class="delete" style="float: right;">
								<form action="commentdelete" method="post">
								<input name="comment_id" value="${comment.id}" id="comment_id" type="hidden"/>
								<input type="submit" value="削除"  onClick= "return deleteCheck()"></form>
							</div>
							</c:if>
			            </div>
			        </c:if>
		    		</c:forEach>
		    		</div>
	            </div>
    		</c:forEach>
    	</div>
    	<div class="copyright"> Copyright(c)SawadaYuka</div>
</div>
</body>
</html>
